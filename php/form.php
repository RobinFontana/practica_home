<div class="formulario">
    <form id="contact-form" action="" method="post"> 
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="name"  name="name" placeholder="Escriba su nombre">
        </div>
        <div class="form-group">
            <label for="nombre">Apellidos</label>
            <input type="text" class="form-control" id="lastname"  name="lastname" placeholder="Escriba su apellido">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email"  name="email"  placeholder="Escriba su email">
        </div>
        <div class="form-group">
            <label for="email">Repita su email</label>
            <input type="email" class="form-control" id="email-chek"  name="email-chek"  placeholder="Repita su email">
        </div>
        <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" id="password" name="password"  placeholder="introduzca su contraseña">
        </div>
        <div class="form-group">
            <label for="password">Repita su contraseña</label>
            <input type="password" class="form-control" id="password-chek" name="password-chek"  placeholder="Repita su contraseña">
        </div>
        <div class="form-group">
            <label for="comunidad">comunidad autonoma</label>
            <select class="form-control" id="comunidad" name="comudidad[]" >
            <option value="Bal">Baleares</option>
            <option value="Cat">Cataluña</option>
            <option value="Val">Comunidad Valenciana</option>
            <option value="And">Andalucia</option>
            <option value="Mad">Madrid</option>
            </select>
        </div>

        <table id="mitabla" class="display">
            <thead>
                <tr>
                    <th>Juegos</th>
                    <th>Precio</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Mario Plataformas</td>
                    <td>5000€</td>
                </tr>
                <tr>
                    <td>Sonic Plataformas</td>
                    <td>5€</td>
                </tr>
                <tr>
                    <td>Mundo Pikachu</td>
                    <td>60€</td>
                </tr>
                <tr>
                    <td>Golden Sun</td>
                    <td>10,000€</td>
                </tr>
                <tr>
                    <td>Hit and Run</td>
                    <td>2€</td>
                </tr>
                <tr>
                    <td>GTA 3</td>
                    <td>200€</td>
                </tr>
            </tbody>
        </table>

        <textarea name="ckeditor" id="ckeditor" rows="10" cols="40"></textarea>

        <button id="submit"  class="btn btn-primary">Submit</button>	
    </form>
</div>