<div class="parallax">
    <div class="transbox">
        <h1>This is some text that is placed in the transparent box.</h1>
    </div>
</div>

<div class="parallax_title">
<h1>Parallax</h1>
</div>

<div class="parallax_content" >
Scroll Up and Down this page to see the parallax scrolling effect.
This div is just here to enable scrolling.
Tip: Try to remove the background-attachment property to remove the scrolling effect.
</div>

<div class="parallax"></div>