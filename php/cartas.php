
    <section>
        <div class="col-lg-12 novedades">		
            
            <h3 align="center" class="col-12">Novedades</h3>
                
            <div class="cards row">

                <div class="card col-5 col-lg-2">
                    <img src="img/mortal kombat.jpg" alt="Avatar" >
                    <div class="container">
                        <h4><b>Mortal Kombat</b></h4> 
                        <p>Lucha</p> 
                    </div>
                </div>
                        
                <div class="card col-5 col-lg-2">
                    <img src="img/mario.jpg" alt="Avatar">

                    <div class="container">
                        <h4><b>Mario Bros</b></h4> 
                        <p>Plataforma</p> 
                    </div>
                </div>
                            
                <div class="card col-5 col-lg-2">
                    <img src="img/gorilas.jpg" alt="Avatar" >

                    <div class="container">
                        <h4><b>Donkey Kong</b></h4> 
                        <p>Plataforma</p> 
                    </div>
                </div>
                        
                <div class="card col-5 col-lg-2">
                    <img src="img/aspiri.jpg" alt="Avatar" >
                    
                    <div class="container">
                        <h4><b>Aspiri</b></h4> 
                        <p>Colección</p> 
                    </div>
                </div>
                        
                <div class="card col-5 col-lg-2">
                    <img src="img/sonic.jpg" alt="Avatar" >
                    <div class="container">
                        <h4><b>Sonic</b></h4> 
                        <p>Plataforma</p> 
                    </div>
                </div>

                <div class="card col-5 col-lg-2">
                    <img src="img/outrun.jpg" alt="Avatar" >
                    <div class="container">
                        <h4><b>Out Run</b></h4> 
                        <p>Carreras</p> 
                    </div>
                </div>

            </div>	
            
        </div>	
    </section>
