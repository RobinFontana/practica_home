


   // Prevenimos que el boton de submit funcione antes de que se rellenen los campos correctamente
    $("#contact-form").submit(function(event) {
        event.preventDefault();
    

        // declaramos la funcion lettersonly que sirve para que solo se admitan letras.
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-záéíóúàèìòùäëïöüñ\s]+$/i.test(value);
            }, "Sólo se aceptan letras");
            
        $("#contact-form").validate({

            rules:{
                name: {
                    required: true,
                    minlength:4,
                    maxlength:20,
                    lettersonly: true,
                },
                lastname: {
                    required: true,
                    minlength:4,
                    maxlength:20,
                    lettersonly: true, 
                },
                email: {
                    required: true,
                    minlength:7,
                    maxlength:34,
                    lettersonly: false, 
                    equalTo:"#email-chek",
                },
                password: {
                    required: true,
                    minlength:12,
                    maxlength:34,
                    lettersonly: false, 
                    equalTo:"#password-chek",
                },
                ckeditor:{
                    required: true,
                },
            },

            messages:{
                name: {
                    required:"Campo obligatorio",
                    minlength:"Debe contener 4 caracteres como minimo",
                    maxlength:"Maximo 20 caracteres",
                    lettersonly:"Se requieren letras",
                },
                lastname: {
                    required:"Campo obligatorio",
                    minlength:"Debe contener 4 caracteres como minimo",
                    maxlength:"Maximo 20 caracteres",
                    lettersonly:"Se requieren letras",
                },
                email: {
                    required:"Campo obligatorio",
                    minlength:"Debe contener 7 caracteres como minimo",
                    maxlength:"Maximo 34 caracteres",
                    equalTo:"Los campos no coinciden",
                },
                password: {
                    required:"Campo obligatorio",
                    minlength:"Debe contener 12 caracteres como minimo",
                    maxlength:"Maximo 34 caracteres",
                    equalTo:"Los campos no coinciden",
                },
                ckeditor:{
                    requiredd:"animate a escribir algo"
                }
            },
        });
        if($("#contact-form").valid()){
            
            //Metemos todo el formulario dentro de un objeto
            var formData = new FormData($("#contact-form")[0]);
            //Variable para los datos del texto
            var textData = CKEDITOR.instances.ckeditor.getData();
            //Podemos añadirle más datos "manualmente" con append (como la de ckeditor)
            formData.append("ckeditor",textData);
        
            for(var pair of formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]); 
            }

            $.ajax({
                type: 'post',
                url: "/foo/bar.html",
                dataType: 'text',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
    
                success: function (response) {
                $(".corrector").html("Gracias " + $("#name").val());        
                },
    
                error: function(response){
                $(".correctorError").html("No Funciona");    
                }
            
            });    
        }
                
    });

    var dataSet = [
        ["Mario Plataformas","5000€"],
        ["Sonic Plataformas","5€"],
        ["Mundo Pikachu","60€"],
        ["Golden Sun","10,000€"],
        ["Hit and Run","2€"],
        ["GTA 3","200€"],
    ]

    $(document).ready(function() {
        $('#comunidad').select2();
        $('#mitabla').DataTable( {
            data: dataSet,
            columns: [
                { title: "Juegos" },
                { title: "Precio" },
            ]
        });
    });

    CKEDITOR.replace('ckeditor');

    $(document).ready( function () {
        $('#mitabla').DataTable();

        $.mockjax({
            url : "/foo/bar.html",
            responseText : 'Server Response Emulated'
        });


        if ($('.select2').length < 0){
            $('.select2').select2({
                language:"es",
            });   
        }
    });    